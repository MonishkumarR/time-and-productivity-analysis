   <style>
   #stopWatch {  height: auto; text-align: center; display: block; padding: 5px; margin: 0 auto; }
               #timer, #fulltime { width: auto; height: auto; padding: 10px; font-weight: bold; font-family: tahoma; display: block; border: 1px solid #eee; text-align: center; box-shadow: 0 0 5px #ccc; background: #fbfbf0; color: darkblue; border-bottom:4px solid darkgrey; }
               button { cursor: pointer; font-weight: 700; }
               #fulltime { display:none; font-size:16px; font-weight:bold; } 
               .title{
                text-align: center;
                color: #2184cd;
               }</style>
  <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="myprofile.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Time and Productivity</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="dashboard.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Profile</span></a>

      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
    

    

      <!-- Nav Item - Utilities Collapse Menu -->
     

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="home.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Home</span></a>
      </li>
  
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="projects.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Projects</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="blog.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Blog</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="timeline.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Timeline</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="report.php">
          <i class="fas fa-fw fa-table"></i>
          <span>Reports</span></a>
      </li>
      <li class="nav-item">
       <h1 class="title">Timer</h1>
        <section id="stopWatch"> 
            <p id="timer"> Time : 00:00:00 </p> 
            <button id="start"> Start </button> 
            <button id="stop"> Stop </button> 
            <p id="fulltime"> </p> 
        </section>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <script type="text/javascript"> 
        // initialize your variables outside the function 
        var count = 0; var clearTime; var seconds = 0, minutes = 0, hours = 0; 
        var clearState; var secs, mins, gethours ; 
        function startWatch( ) { 
            /* check if seconds is equal to 60 and add a +1 to minutes, and set seconds to 0 */ 
            if ( seconds === 60 ) { seconds = 0; minutes = minutes + 1; }
             /* you use the javascript tenary operator to format how the minutes should look and add 0 to minutes if less than 10 */
              mins = ( minutes < 10 ) ? ( '0' + minutes + ': ' ) : ( minutes + ': ' ); 
              /* check if minutes is equal to 60 and add a +1 to hours set minutes to 0 */ 
              if ( minutes === 60 ) { minutes = 0; hours = hours + 1; } 
              /* you use the javascript tenary operator to format how the hours should look and add 0 to hours if less than 10 */ 
              gethours = ( hours < 10 ) ? ( '0' + hours + ': ' ) : ( hours + ': ' );
              secs = ( seconds < 10 ) ? ( '0' + seconds ) : ( seconds ); 
              // display the stopwatch 
              var x = document .getElementById("timer"); x.innerHTML = 'Time: ' + gethours + mins + secs; 
              /* call the seconds counter after displaying the stop watch and increment seconds by +1 to keep it counting */ 
              seconds++; 
              /* call the setTimeout( ) to keep the stop watch alive ! */ 
              clearTime = setTimeout( "startWatch( )", 1000 ); } 
              // startWatch( ) //create a function to start the stop watch 
        function startTime( ) { 
            /* check if seconds, minutes, and hours are equal to zero and start the stop watch */ 
            if ( seconds === 0 && minutes === 0 && hours === 0 ) { 
                /* hide the fulltime when the stop watch is running */ 
                var fulltime = document.getElementById( "fulltime" ); 
                fulltime.style.display = "none"; 
                /* hide the start button if the stop watch is running */ 
                this.style.display = "none"; 
                /* call the startWatch( ) function to execute the stop watch whenever the startTime( ) is triggered */ 
                startWatch( ); } 
                // if () 
            } // startTime() 
                /* you need to bind the startTime( ) function to any event type to keep the stop watch alive ! */



        //create a function to stop the time 
        function stopTime( ) { 
            /* check if seconds, minutes and hours are not equal to 0 */ 
            if ( seconds !== 0 || minutes !== 0 || hours !== 0 ) {
                 /* display the full time before reseting the stop watch */ 
                 var fulltime = document .getElementById( "fulltime" ); 
                 //display the full time 
                 fulltime.style.display = "block"; 
                 var time = gethours + mins + secs; fulltime.innerHTML = 'Fulltime: ' + time; 
                 // reset the stop watch 
                 seconds = 0; minutes = 0; hours = 0; secs = '0' + seconds; mins = '0' + minutes + ': '; gethours = '0' + hours + ': '; 
                 /* display the stopwatch after it's been stopped */ 
                 var x = document.getElementById ("timer"); 
                 var stopTime = gethours + mins + secs; 
                 x.innerHTML = stopTime; 
                 /* display all stop watch control buttons */ 
                 var showStart = document.getElementById ('start'); 
                 showStart.style.display = "inline-block"; 
                 var showStop = document.getElementById ("stop"); 
                 showStop.style.display = "inline-block"; 
                 /* clear the stop watch using the setTimeout( ) return value 'clearTime' as ID */ 
                 clearTimeout( clearTime ); } 
                 // if () 
                } // stopTime() 
                 /* you need to call the stopTime( ) function to terminate the stop watch */ 
                 window.addEventListener( 'load', function ( ) { 
                     var stop = document.getElementById ("stop");
                      stop.addEventListener( 'click', stopTime );
                      var start = document .getElementById("start"); 
                      start.addEventListener( 'click', startTime );
                     }); 
        </script>