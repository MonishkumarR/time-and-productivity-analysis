<?php
session_start();
include('includes/dbconnection.php');
if(strlen($_SESSION['uid']==0)){
	header('location:logout.php');
}
else {
?>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$db = "graph";
$conn = mysqli_connect($servername, $username, $password,$db);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
else
{
//echo "Connected successfully";
}  
$sql = "SELECT date as count FROM date";

$result = mysqli_query($conn,$sql);

    $result = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $result = json_encode(array_column($result, 'count'),JSON_NUMERIC_CHECK);
    ?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Welcome to ERMS</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
  

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script>
     var data_click = <?php echo $result; ?>;
     var today = new Date();
     var c=parseInt(data_click[0][9])+parseInt((data_click[0][8]*10));
    // document.write(c);
     var n = today.getDate();
     //alert(today);
         if(c==n)
         {
          alert("raghul");
         }
     //document.write(n);
      
</script>
<style>

.flex-wrapper {
  display: flex;
  flex-flow: row nowrap;
}

.single-chart {
  width: 33%;
  justify-content: space-around ;
}

.circular-chart {
  display: block;
  margin: 10px auto;
  max-width: 80%;
  max-height: 250px;
}

.circle-bg {
  fill: none;
  stroke: #eee;
  stroke-width: 3.8;
}

.circle {
  fill: none;
  stroke-width: 2.8;
  stroke-linecap: round;
  animation: progress 1s ease-out forwards;
}

@keyframes progress {
  0% {
    stroke-dasharray: 0 100;
  }
}

.circular-chart.orange .circle {
  stroke: #ff9f00;
}

.circular-chart.green .circle {
  stroke: #4CC790;
}

.circular-chart.blue .circle {
  stroke: #3c9ee5;
}
.circular-chart.yellow .circle {
  stroke: yellow;
}

.percentage {
  fill: #666;
  font-family: sans-serif;
  font-size: 0.5em;
  text-anchor: middle;
}



</style>
 
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once('includes/sidebar.php');?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
       <?php include_once('includes/header.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Time and Productivity Analyzer</h1>
                   </div>

          <!-- Content Row -->
          <div class="row">
<div class="col-xl-3 col-md-6 mb-4"></div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-6 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Welcome Coder!</div>

                      <?php
$empid=$_SESSION['uid'];
$ret=mysqli_query($con,"select EmpFname,EmpLname from employeedetail where ID='$empid'");
$row=mysqli_fetch_array($ret);
$fname=$row['EmpFname'];
$lname=$row['EmpLname'];
?>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $fname." ".$lname; ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
             </div>
           <center>  <h1><b>current project :</b> agri developer </h1></center>
<br>
<center><h2><b>status</b>: in progress <span class="w3-badge w3-green">4</span> </h2></center>

<div class="flex-wrapper">

  <div class="single-chart">
    <svg viewBox="0 0 36 36" class="circular-chart orange">
      <path class="circle-bg"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path class="circle"
        stroke-dasharray="90, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" class="percentage">90%</text>
    </svg>
     <header><center><h1>total cost<h1><center></header>
  </div>
   <div class="single-chart">
    <svg viewBox="0 0 36 36" class="circular-chart green">
      <path class="circle-bg"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path class="circle"
        stroke-dasharray="60, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" class="percentage">60%</text>
    </svg>
     <header><center><h1>progress<h1><center></header>
  </div>

  <div class="single-chart">
    <svg viewBox="0 0 36 36" class="circular-chart blue">
      <path class="circle-bg"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path class="circle"
        stroke-dasharray="75, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" class="percentage">7</text>
    </svg>
     <header><center><h1>deadline<h1><center></header>
  </div>
  
  <div class="single-chart">
    <svg viewBox="0 0 36 36" class="circular-chart yellow">
      <path class="circle-bg"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path class="circle"
        stroke-dasharray="48, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" class="percentage">48hrs</text>
    </svg>
     <header><center><h1>total hrs<h1><center></header>
  </div>
</div>

      

<?php

$sql = "SELECT hr as count FROM graph";

$result = mysqli_query($con,$sql);

    $result = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $result = json_encode(array_column($result, 'count'),JSON_NUMERIC_CHECK);



    $sql2 = "SELECT dte as sum from graph";
    $result2 = mysqli_query($con,$sql2);
    $result2 = mysqli_fetch_all($result2,MYSQLI_ASSOC);

    $result2 = json_encode(array_column($result2, 'sum'),JSON_NUMERIC_CHECK);


    $sql3 = "SELECT cs as sum from graph";
    $result3 = mysqli_query($con,$sql3);
    $result3 = mysqli_fetch_all($result3,MYSQLI_ASSOC);

    $result3 = json_encode(array_column($result3, 'sum'),JSON_NUMERIC_CHECK);
    //while($row = $result->fetch_assoc()) {
  //echo "id: " . $row["hours"];
   //$data[] = $row['hours'];
//}
//  echo $result2;
?>

<div id="cost" style="float:right; width:50%;"></div>
<script type="text/javascript">




    var data_click = <?php echo $result3; ?>;
     var data_click2 = <?php echo $result2; ?>;

  Highcharts.chart('cost', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'cost analyzer'
  },
  subtitle: {
    text: 'wings of fire'
  },
  xAxis: {
    categories: data_click2
  },
  yAxis: {
    title: {
      text: 'phases'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: false
    }
  },
  series: [{
    name: 'target cost',
    data: data_click
  }]
});



</script>
<div id="hours" style="float:left; width:45%;"></div>
<script type="text/javascript">




    var data_click = <?php echo $result; ?>;
     var data_click2 = <?php echo $result2; ?>;

  Highcharts.chart('hours', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'hours worked'
  },
  subtitle: {
    text: 'wings of fire'
  },
  xAxis: {
    categories: data_click2
  },
  yAxis: {
    title: {
      text: 'phases'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: false
    }
  },
  series: [{
    name: 'hours worked',
    data: data_click
  }]
});



</script>

             
         
         

          <!-- Content Row -->

        </div>
          

         
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
       <?php include_once('includes/footer.php');?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
<hr>
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>

</html>
<?php
}
?>