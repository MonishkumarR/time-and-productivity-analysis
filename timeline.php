<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);
if (strlen($_SESSION['uid']==0)) {
  header('location:logout.php');
  } else{


  
  ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Reports</title>
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script type="text/javascript">
     function show()
{
    var y = document.getElementById("container").style.display='block';
          var z= document.getElementById("textbox").style.display='none';

    }
    function show2()
{
    var y = document.getElementById("textbox").style.display='block';
     var z= document.getElementById("container").style.display='none';
    }
  </script>
<style type="text/css">
  * {
  box-sizing: border-box;
}

</style>
<style>
.flex-wrapper {
  display: flex;
  flex-flow: row nowrap;
}

.single-chart {
  width: 33%;
  justify-content: space-around ;
}

.circular-chart {
  display: block;
  margin: 10px auto;
  max-width: 80%;
  max-height: 250px;
}

.circle-bg {
  fill: none;
  stroke: #eee;
  stroke-width: 3.8;
}

.circle {
  fill: none;
  stroke-width: 2.8;
  stroke-linecap: round;
  animation: progress 1s ease-out forwards;
}

@keyframes progress {
  0% {
    stroke-dasharray: 0 100;
  }
}

.circular-chart.orange .circle {
  stroke: #ff9f00;
}




.percentage {
  fill: #666;
  font-family: sans-serif;
  font-size: 0.5em;
  text-anchor: middle;
}

</style>
</head>

<body id="page-top" >

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
  <?php include_once('includes/sidebar.php')?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
         <?php include_once('includes/header.php')?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Reports</h1>
          <br>
          
<div class="flex-wrapper">

  <div class="single-chart">

    <svg viewBox="0 0 36 36" class="circular-chart orange">
      <path class="circle-bg"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path class="circle"
        stroke-dasharray="100, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" class="percentage">9000rs</text>
     
    </svg>
        <header><center><h1>estimated cost<h1><center></header>    
  </div>

  <div class="single-chart">

    <svg viewBox="0 0 36 36" class="circular-chart orange">
      <path class="circle-bg"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path class="circle"
        stroke-dasharray="100, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" class="percentage">300hrs</text>
     
    </svg>
        <header><center><h1>estimated hours<h1><center></header>    
  </div>

  <div class="single-chart">

    <svg viewBox="0 0 36 36" class="circular-chart orange">
      <path class="circle-bg"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <path class="circle"
        stroke-dasharray="100, 100"
        d="M18 2.0845
          a 15.9155 15.9155 0 0 1 0 31.831
          a 15.9155 15.9155 0 0 1 0 -31.831"
      />
      <text x="18" y="20.35" class="percentage">july 6</text>
     
    </svg>
        <header><center><h1>estimated dates<h1><center></header>    
  </div>

</div>
<br>

   
 
<p style="font-size:16px; color:red" align="center"> <?php if($msg){
    echo $msg;
  }  ?> </p>
  

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<?php

$sql = "SELECT tcost as count FROM timeline";

$result = mysqli_query($con,$sql);

    $result = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $result = json_encode(array_column($result, 'count'),JSON_NUMERIC_CHECK);



    $sql2 = "SELECT phase as sum from timeline";
    $result2 = mysqli_query($con,$sql2);
    $result2 = mysqli_fetch_all($result2,MYSQLI_ASSOC);

    $result2 = json_encode(array_column($result2, 'sum'),JSON_NUMERIC_CHECK);
    //while($row = $result->fetch_assoc()) {
  //echo "id: " . $row["hours"];
   //$data[] = $row['hours'];
//}
//  echo $result2;
    $sql3 = "SELECT acost as sum from timeline";
    $result3 = mysqli_query($con,$sql3);
    $result3 = mysqli_fetch_all($result3,MYSQLI_ASSOC);

    $result3 = json_encode(array_column($result3, 'sum'),JSON_NUMERIC_CHECK);
?>

<div id="cost" style="float:right; width:50%;"></div>
<script type="text/javascript">




    var data_click = <?php echo $result; ?>;
     var data_click2 = <?php echo $result2; ?>;
     var data_click3 = <?php echo $result3; ?>;

  Highcharts.chart('cost', {
  chart: {
    type: 'line'
  },
  title: {
    text: 'cost analyzer'
  },
  subtitle: {
    text: 'wings of fire'
  },
  xAxis: {
    categories: data_click2
  }
,  yAxis: {
    title: {
      text: 'phases'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: false
    }
  },
 series:  [{
    name: 'target time',
    data: data_click
  }, {
    name: 'current time',
    data: data_click3
  }]
});



</script>
<div id="hours" style="float:left; width:45%;"></div>
<script type="text/javascript">




    var data_click = <?php echo $result; ?>;
     var data_click2 = <?php echo $result2; ?>;

  Highcharts.chart('hours', {
  chart: {
    type: 'line'
  },
  title: {
    text: 'hours worked'
  },
  subtitle: {
    text: 'wings of fire'
  },
  xAxis: {
    categories: data_click2
  },
  yAxis: {
    title: {
      text: 'phases'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: false
    }
  },
 series: [{
    name: 'target time',
    data: [32,48,24,48,12,10]
  }, {
    name: 'current time',
    data: [34,50,48,32,18,8]
  }]
});



</script>





        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
   <?php include_once('includes/footer.php');?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

    <i class="fas fa-angle-up"></i>
  </a>

  

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script type="text/javascript">
    $(".jDate").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
}).datepicker("update", "10/10/2016"); 
  </script>

</body>
 <div id="textbox" style="display: none">
<strong>IF DAY SCHOLAR:</strong>
BOARDING POINT:<input name="boardingpt" type="text" >
BUS NO:<input name="bus" type="number" ><br>
</div>
</html>
<?php }  ?>
