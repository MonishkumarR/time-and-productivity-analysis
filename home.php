<?php
session_start();
include('includes/dbconnection.php');




if(mysqli_connect_errno()){
echo "Connection Fail".mysqli_connect_error();
}


if(strlen($_SESSION['uid']==0)){
  header('location:logout.php');
}
else {
 
  
  
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Welcome to ERMS</title>

  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->

  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <style type="text/css">
  
.flex-wrapper {
  display: flex;
  flex-flow: row nowrap;

  
  }

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  width: 320px;
  height: 400px;
  margin: auto;
  text-align: center;
  font-family: arial;
}

.title {
  color: grey;
  font-size: 18px;
}

button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 80%;
  font-size: 18px;
}

img {
  border-radius: 5px 5px 0 0;
}


a {
  text-decoration: none;
  font-size: 22px;
  color: black;
}

button:hover, a:hover {
  opacity: 0.7;
}

.flip-card {

  width: 320px;
  height: 400px;
  margin: auto;
  text-align: center;
  perspective: 1000px;
}

.flip-card-inner {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.6s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}

.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
}

.flip-card-front, .flip-card-back {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}

.flip-card-front {
  background-color: #bbb;
  color: black;
  z-index: 2;
}

.flip-card-back {
  background-color: #2980b9;
  color: white;
  transform: rotateY(180deg);
  z-index: 1;
}

body {font-family: Arial, Helvetica, sans-serif;}

/* Full-width input fields */
label{
    display: inline-block;
   
    width: 150px;
    height:25px;
    text-align: left; /*Change to right here if you want it close to the inputs*/
}

input {
  width: 70%;
  padding: 11px 18px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
  background-color: lightblue;
 

  /*
  width: 85%;
  padding: 12px 18px;
  margin:8px 0;
    border: 1px solid #ccc;
    box-sizing: border-box;
  display: inline-block;
  float: left;
  clear:left;*/
}

/* Set a style for all buttons */
#button {
  background-color: blue;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

#button:hover {
  opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
  position: relative;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.container {
  padding: 20px;
  width:100%;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
  position: absolute;
  right: 25px;
  top: 0;
  color: #000;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: red;
  cursor: pointer;
}

/* Add Zoom Animation */
.animate {
  -webkit-animation: animatezoom 0.6s;
  animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
  from {-webkit-transform: scale(0)} 
  to {-webkit-transform: scale(1)}
}
  
@keyframes animatezoom {
  from {transform: scale(0)} 
  to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
#buttonsize
  {
    margin-left: 29%;
    text-align: center;
    width:150px;
    height:30px;
    background-color: lightgreen;
  }




  </style>
  
</head>
<div class="cd-bg-video-wrapper" data-video="video/bangkok-city">
            <!-- video element will be loaded using jQuery -->
        </div>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php include_once('includes/sidebar.php');?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
       <?php include_once('includes/header.php');?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Time and Productivity Analyzer</h1>
                   </div>

          <!-- Content Row -->
          <div class="row">
<div class="col-xl-3 col-md-6 mb-4"></div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-6 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Welcome Coder!</div>

                      <?php
$empid=$_SESSION['uid'];
$ret=mysqli_query($con,"select EmpFname,EmpLname from employeedetail where ID='$empid'");
$row=mysqli_fetch_array($ret);
$fname=$row['EmpFname'];
$lname=$row['EmpLname'];
?>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $fname." ".$lname; ?></div>
                      level 1 badge 30
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        

         
          </div>

          <!-- Content Row -->

        </div>
           <!--card-->
                  <div class="flex-wrapper">
                  
<div class="card">
  <img src="img\uu.png" alt="current" style="width:300px">
  <h3>current project</h3>
  <p class="title">agri developer<br></p>
  <p>
phase completed  <span class="w3-badge w3-green">4</span><br>
yet to complete<span class="w3-badge w3-red">3</span>
</p>

  
 </div>
<div class="card">
  <img src="img\ff.png" alt="John" style="width:100%">
  <h3> project 1</h3>
  <p class="title">socio developer</p>
 
  
  <p><button>proceed</button></p>
</div>

<div class="flip-card">
  <div class="flip-card-inner">
    <div class="flip-card-front">
      <img src="img\pp.jpg" alt="Avatar" style="width:320px;height:400px;">
    </div>
    <div class="flip-card-back">
      <h1>socio app</h1> 
      <p>completed in feb 26</p> 
      <p>report : good</p>
    </div>
  </div>

</div>

</div>
<center><h2>Modal Login Form</h2></center>

<img src="img\com.png" alt="Avatar" style="width:100px;height:70px;"><button  onclick="document.getElementById('id01').style.display='block'" style="width:auto;">new project</button>

<div id="id01" class="modal">
  
  <form class="modal-content animate" method="post" action="task.php">
   <!-- <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
      <img src="img_avatar2.png" alt="Avatar" class="avatar">
    </div>
-->
    <div class="container">
      <h1>Project Details</h1>
      <label for="pname"><b>Project</b></label>
      <input style="color:red;" type="text" placeholder="Enter projectname" name="name" id="name" required><br>
      <label for="task"><b>TaskList</b></label>
      <input type="number" placeholder="Enter task" name="phases" ><br>
      
      <!--<button onclick="">ADDTASK</button>      
      <button id="myButton" class="float-left submit-button" >Home</button>

<script type="text/javascript">
    document.getElementById("myButton").onclick = function () {
        location.href = "s.html";
    };-->
    
       
      <label for="date"><b>Set-Date</b></label>
      <input type="date" placeholder="start date" name="sdate" ><br>
      <label></label>
      <input type="date" placeholder="Target date" name="tdate" ><br>


       <label for="billinfo"><b>Billing</b></label>
      <input type="number" placeholder="billinfo" name="cost" >
      <br>


       <label for="time"><b>no.of persons</b></label>
      <input type="number" placeholder="TARGET PERSONS" name="persons" ><br>
      <label></label>
      

      <input type="submit" name="submit"></button>
    </div>
  </form>
</div>

<script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

         
          </div>


        </div>
        


        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
       <?php include_once('includes/footer.php');?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>

</html>
<?php
}
?>