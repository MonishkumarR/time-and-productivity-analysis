<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);
if (strlen($_SESSION['uid']==0)) {
  header('location:logout.php');
  } else{


if(isset($_POST['submit']))
  {
    $eid=$_SESSION['uid'];
      $coursepg=$_POST['coursepg'];
    $schoolclgpg=$_POST['schoolclgpg'];
    $yoppg=$_POST['yoppg'];
    $pipg=$_POST['pipg'];
    $coursegra=$_POST['coursegra'];
    $schoolclggra=$_POST['schoolclggra'];
    $yopgra=$_POST['yopgra'];
    $pigra=$_POST['pigra'];
    $coursessc=$_POST['coursessc'];
    $schoolclgssc=$_POST['schoolclgssc'];
    $yopssc=$_POST['yopssc'];
    $pissc=$_POST['pissc'];
    $coursehsc=$_POST['coursehsc'];
    $schoolclghsc=$_POST['schoolclghsc'];
    $yophsc=$_POST['yophsc'];
    $pihsc=$_POST['pihsc'];
    
     $query=mysqli_query($con, "update empeducation set CoursePG='$coursepg', SchoolCollegePG='$schoolclgpg', YearPassingPG='$yoppg',  PercentagePG= '$pipg', CourseGra='$coursegra',  SchoolCollegeGra='$schoolclggra', YearPassingGra= '$yopgra', PercentageGra='$pigra', CourseSSC='$coursessc', SchoolCollegeSSC='$schoolclgssc', YearPassingSSC= '$yopssc', PercentageSSC= '$pissc', CourseHSC='$coursehsc', SchoolCollegeHSC='$schoolclghsc', YearPassingHSC='$yophsc', PercentageHSC='$pihsc' where EmpID='$eid'");
    if ($query) {
    $msg="Your Education data has been updated succeesfully.";
  }
  else
    {
      $msg="Something Went Wrong. Please try again.";
    }
  }
  ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Reports</title>
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script type="text/javascript">
     function show()
{
    var y = document.getElementById("container").style.display='block';
          var z= document.getElementById("textbox").style.display='none';

    }
    function show2()
{
    var y = document.getElementById("textbox").style.display='block';
     var z= document.getElementById("container").style.display='none';
    }
  </script>

</head>

<body id="page-top" >

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
  <?php include_once('includes/sidebar.php')?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
         <?php include_once('includes/header.php')?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Reports</h1>
          <br>
          


    Graph:<input name="schooling" type="radio" value="Graph" id="day" onclick="show()"><br>
    Summary:<input name="schooling" type="radio" value="Graph" id="day" onclick="show2()">
 
<p style="font-size:16px; color:red" align="center"> <?php if($msg){
    echo $msg;
  }  ?> </p>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<?php
$servername = "localhost";
$username = "root";
$password = "";
$db = "graph";
$conn = mysqli_connect($servername, $username, $password,$db);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
else
{
echo "Connected successfully";
}
$sql = "SELECT hours as count FROM graph";

$result = mysqli_query($conn,$sql);

    $result = mysqli_fetch_all($result,MYSQLI_ASSOC);

    $result = json_encode(array_column($result, 'count'),JSON_NUMERIC_CHECK);



    $sql2 = "SELECT day as sum from graph";
    $result2 = mysqli_query($conn,$sql2);
    $result2 = mysqli_fetch_all($result2,MYSQLI_ASSOC);

    $result2 = json_encode(array_column($result2, 'sum'),JSON_NUMERIC_CHECK);
    //while($row = $result->fetch_assoc()) {
  //echo "id: " . $row["hours"];
   //$data[] = $row['hours'];
//}
//  echo $result2;
?>
<div id="container"></div>
<script type="text/javascript">




    var data_click = <?php echo $result; ?>;
     var data_click2 = <?php echo $result2; ?>;

  Highcharts.chart('container', {
  chart: {
    type: 'line'
  },
  title: {
    text: 'Time analyzer'
  },
  subtitle: {
    text: 'wings of fire'
  },
  xAxis: {
    categories: data_click2
  },
  yAxis: {
    title: {
      text: 'day'
    }
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true
      },
      enableMouseTracking: false
    }
  },
  series: [{
    name: 'target time',
    data: data_click
  }, {
    name: 'current time',
    data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2]
  }]
});



</script>





        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
   <?php include_once('includes/footer.php');?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

    <i class="fas fa-angle-up"></i>
  </a>

  

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script type="text/javascript">
    $(".jDate").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
}).datepicker("update", "10/10/2016"); 
  </script>

</body>
 <div id="textbox" style="display: none">
<strong>IF DAY SCHOLAR:</strong>
BOARDING POINT:<input name="boardingpt" type="text" >
BUS NO:<input name="bus" type="number" ><br>
</div>
</html>
<?php }  ?>
