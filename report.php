<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
//error_reporting(0);
if (strlen($_SESSION['uid']==0)) {
  header('location:logout.php');
  } else{


  ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>report</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <style>
    table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
  </style>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
  <?php include_once('includes/sidebar.php')?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
         <?php include_once('includes/header.php')?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Description</h1>

<p style="font-size:16px; color:red" align="center"> <?php if($msg){
    echo $msg;
  }  ?> </p>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
<script type="text/javascript">
Highcharts.chart('container', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'project phases'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: false
      },
      showInLegend: true
    }
  },
  series: [{
    name: 'Brands',
    colorByPoint: true,
    data: [{
      name: 'documentation',
      y: 35,
     sliced: true,
      //selected: true
    }, {
      name: 'execution',
      y: 15
    }, {
      name: 'planning',
      y: 10
    }, {
      name: 'analysis',
      y: 7
    }, {
      name: 'testing',
      y: 23
    }, {
      name: 'marketing',
      y: 10
    }]
  }]
});
</script>
<table>
  <tr>
    <th>agri project</th>
    <th>in progress</th>
    <th>completion 99%</th>
      <th>dead line feb 7</th>
      
  </tr>
</table>
<table>
  <tr>
    <th>Phases</th>
    <th>target time</th>
    <th>real time</th>
      <th>exceeded</th>
      <th>cost</th>
  </tr>
  <tr>
    <td>implementation</td>
    <td>feb1 </td>
    <td>feb 1</td>
    <td>-</td>
    <th>2000rs</th>
  </tr>
  <tr>
    <td>planning</td>
    <td>feb 3</td>
    <td>feb 4</td>
    <td>1</td>
    <th>1000rs</th>
  </tr>
  <br>
  <br>
  <tr>


</table>
 <br>
  <br>
<table>
<tr>
<td>total days</td>
<td> 6 days</td>
</tr>
<tr>
<td>total cost</td>
<td> 3000rs</td>
</tr>
<tr>
<td>average cost</td>
<td> 500/day</td>
</tr>
<tr>
<td>total hours worked</td>
<td> 48hrs</td>
</tr>
<tr>
<td>average time</td>
<td>  8hrs</td>
</tr>
<tr>
<td>target time exceeded by</td>
<td> 3 days</td>
</tr>
<tr>
<td>total person</td>
<td> 6 </td>
</tr>
<tr>
<td>average production ratio</td>
<td> 8(time ratio)</td>
<td> 500rs(cost ratio)</td>
</tr>
</table>





        </div>
        <!-- /.container-fluid -->



      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
   <?php include_once('includes/footer.php');?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script type="text/javascript">
    $(".jDate").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
}).datepicker("update", "10/10/2016"); 
  </script>
<html>
<head>
<style>
  table,th,td
  {
    border: 1px solid black;
    padding: : 5px;
  }
</style>
</head>
<body>
<?php 
$con=mysqli_connect("localhost", "root", "", "ermsdb");
if(mysqli_connect_errno()){
echo "Connection Fail".mysqli_connect_error();
}
$arr1=[];
$arr2=[];
$arr3=[];
$int1="";
$str="";
$i=0;
$j=0;
$z=0;
exec("tasklist.exe/fo csv /nh",$arr1,$int1);  
for($i=0;$i<count($arr1);$i+=1)
{
  $z=0;
  for($j=0;$j<strlen($arr1[$i]);$j+=1)
  {
    if($arr1[$i][$j]=='"')
    {
      $z+=1;
      continue;
    }
    if($z!=0 && $arr1[$i][$j]!=',')
    {
    $arr2[$i][$j]=$arr1[$i][$j];
      }
      if($z==2)
      {
        break;
      }

  }
}
$k=0;
for($x=0;$x<count($arr2);$x+=1)
{
  for($z=$x+1;$z<count($arr2);$z+=1)
  {
      if(($arr2[$x]==$arr2[$z])&&($arr2[$x]!="***"))
      {
        $arr2[$z]="***";
      }
  }
}
echo "<table>";
echo "<th>Application running</th>";
for($x=0;$x<count($arr2);$x+=1)
{
  if($arr2[$x]!="***")
  {
    echo "<tr>";
    echo "<td>".implode(" ",$arr2[$x])."</td>";

  echo "</tr>";
  }
}
echo "</table>";
?>
</body>
</html>
</body>

</html>
<?php }  ?>
