<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* Full-width input fields */
label{
    display: inline-block;
   
    width: 150px;
    height:25px;
    text-align: left; /*Change to right here if you want it close to the inputs*/
}

input {
  width: 70%;
  padding: 11px 18px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
  background-color: lightgreen;
 

  /*
  width: 85%;
  padding: 12px 18px;
  margin:8px 0;
    border: 1px solid #ccc;
    box-sizing: border-box;
  display: inline-block;
  float: left;
  clear:left;*/
}

/* Set a style for all buttons */
button {
  background-color: blue;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
  position: relative;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.container {
  padding: 20px;
  width:100%;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
  position: absolute;
  right: 25px;
  top: 0;
  color: #000;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: red;
  cursor: pointer;
}

/* Add Zoom Animation */
.animate {
  -webkit-animation: animatezoom 0.6s;
  animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
  from {-webkit-transform: scale(0)} 
  to {-webkit-transform: scale(1)}
}
  
@keyframes animatezoom {
  from {transform: scale(0)} 
  to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
 #buttonsize
  {
    margin-left: 29%;
    text-align: center;
    width:150px;
    height:30px;
    background-color: lightgreen;
  }
</style>
</head>
<body>

<h2>Modal Login Form</h2>

<button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Enter task</button>

<div id="id01" class="modal">
  
  <form class="modal-content animate" action="/action_page.php">
   <!-- <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
      <img src="img_avatar2.png" alt="Avatar" class="avatar">
    </div>
-->
    <div class="container">
      <h1>Project Details</h1>
      <label for="pname"><b>Project</b></label>
      <input style="color:red;" type="text" placeholder="Enter projectname" name="pname" required><br>
      <label for="task"><b>TaskList</b></label>
      <input type="number" placeholder="Enter task" name="task" required><br>
      
      <!--<button onclick="">ADDTASK</button>      
      <button id="myButton" class="float-left submit-button" >Home</button>

<script type="text/javascript">
    document.getElementById("myButton").onclick = function () {
        location.href = "s.html";
    };-->
    
       
      <label for="date"><b>Set-Date</b></label>
      <input type="date" placeholder="start date" name="sdate" required><br>
      <label></label>
      <input type="date" placeholder="Target date" name="tdate" required><br>


       <label for="billinfo"><b>Billing</b></label>
      <input type="text" placeholder="billinfo" name="bill" required>
      <br>


       <label for="time"><b>Alerts</b></label>
      <input type="text" placeholder="Due Date date" name="ddate" required><br>
      <label></label>
      <input type="text" placeholder="Time-Budget-date" name="tdate" required><br>
      <label></label>
      <button id="buttonsize">submit</button>
           
          
    </div>

  </form>
</div>

<script>
// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

</body>
</html>
